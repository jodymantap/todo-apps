import './App.css';
import React, { useState, useEffect } from "react"

function App() {
  const [todo, setTodo] = useState("");
  const [arr, setArr] = useState([]);
  const dataJson = [
    {
      name: "Memasak",
      status: true,
      checkbox: false, 
      edited: false
    },
    {
      name: "Bercocok Tanam",
      status: true,
      checkbox: false, 
      edited: false
    },
    {
      name: "Olahraga",
      status: true,
      checkbox: false, 
      edited: false
    },
  ]

  useEffect(() => {
    if (arr.length <= 0) {
      console.log(JSON.stringify(dataJson), dataJson, "data")
      setArr(JSON.parse(JSON.stringify(dataJson)))
    }
  }, [] )

  const removeKey = async (e) => {
    if (e == "checked") {
      console.log(arr, "checked")
      const newTodos = await arr.filter((item, i) => item["checkbox"] !== true);
      setArr(newTodos);
    } else {
      const newTodos = await arr.filter((item) => item !== arr[e]);
      setArr(newTodos);
    }
    
  }

  const updateInput = (e) => {
    const change = [...arr]
    change[e]["checkbox"] = !change[e]["checkbox"]
    setArr(change)
    console.log(change)
  }

  const handleChange = (e, x = null) => {
    const { name, value } = e.target
    setTodo(value)
  }
  return (
    <div className="App">
      <div className="container">
        <div className="row">
            <div className="col-sm">
                <br></br>
                <h1 className="todolistapp">Todo List App</h1>
                <br></br>
                <br></br>
                <br></br>
                <div className="input-group mb-3">
                  <input name="todoText" onChange={(e) => {
                    handleChange(e)
                  }} type="text" className="form-control" placeholder="What are we gonna do, mate?" value={todo} aria-label="Recipient's username" aria-describedby="button-addon2"></input>
                  <div className="input-group-append">
                    <button className="btn btn-secondary" type="button" id="button-addon2">Add</button>
                    <button onClick={() => removeKey("checked")} className="btn btn-outline-secondary" type="button" id="button-addon2">Delete</button>
                  </div>
                </div>
                {Object.keys(arr).map((keyName, i) => (
                  <div className="d-flex justify-content-center">
                    
                      <input className=""
                      key={arr[keyName]["name"] + "_" + i}
                      onClick={(e) => updateInput(i)}
                      type="checkbox"
                      defaultChecked={arr[keyName]["checkbox"]}
                        />
                        <span>&nbsp; &nbsp;</span>
                      <h5 className="text-secondary"> {arr[keyName]["name"]} <span><button onClick={() => removeKey(i)} className="btn btn-outline-secondary"><i className="fa fa-minus-circle"></i></button></span></h5>
                      <br></br>
                      <br></br>
                      <br></br>
                  </div>
                  ))}
                <br></br>
                <br></br>
                <br></br>
            </div>
        </div>
      </div>
    </div>
    
  );
}

export default App;
